package main

import (
	"fmt"
	"log"
	"math/rand"
	"net/url"
	"os"
	"strconv"
	"strings"
	"time"
)

// Helper function to accept and format both the project ID or name as project
// identifier for all API calls.
// Stolen from https://github.com/xanzy/go-gitlab
func parseID(id interface{}) (string, error) {
	switch v := id.(type) {
	case int:
		return strconv.Itoa(v), nil
	case string:
		return v, nil
	default:
		return "", fmt.Errorf("invalid ID type %#v, the ID must be an int or a string", id)
	}
}

// Helper function to escape a project identifier.
// Stolen from https://github.com/xanzy/go-gitlab
func pathEscape(s string) string {
	return strings.Replace(url.PathEscape(s), ".", "%2E", -1)
}

func IsFinished(status string) bool {
	finishedStates := map[string]bool{
		"failed":    true,
		"cancelled": true,
		"canceled":  true,
		"success":   true,
		"manual":    true,
		"skipped":   true,
	}

	return finishedStates[status]
}

// Golang env values are string, if they're supposed to be int, we need to convert them.
func GetEnvInt(envVar string, defaultValue int) int {
	valueStr, ok := os.LookupEnv(envVar)
	if ok {
		if value, err := strconv.Atoi(valueStr); err == nil {
			return value
		}
	}
	return defaultValue
}

func GetEnvBool(envVar string, defaultValue bool) bool {
	valueStr, ok := os.LookupEnv(envVar)
	if ok {
		if value, err := strconv.ParseBool(valueStr); err == nil {
			return value
		}
	}
	return defaultValue
}

func ValidateEnv(env string) (string, error) {

	value, ok := os.LookupEnv(env)
	if !ok {
		return value, fmt.Errorf("var %s is not set! value: %q", env, value)
	}
	if value == "" {
		return value, fmt.Errorf("var %s is empty! value: %q", env, value)
	}

	return value, nil
}

// Perform recursive retries until don't have anymore
// Inspired by this blog https://upgear.io/blog/simple-golang-retry-function/
func Retry(attempts int, sleep time.Duration, f func() error) error {
	if err := f(); err != nil {
		if attempts--; attempts > 0 {
			log.Printf("Retrying attempt %v", attempts)
			// Add some randomness to prevent creating a Thundering Herd
			jitter := time.Duration(rand.Int63n(int64(sleep)))
			sleep = sleep + jitter/2
			log.Printf("Waiting %v seconds", sleep.Seconds())
			time.Sleep(sleep)

			return Retry(attempts, 2*sleep, f)
		}
		log.Printf("Failed after max retries!")
		return err
	}

	return nil
}
