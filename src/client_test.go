package main

import (
	"errors"
	"fmt"
	"net/http"
	"testing"
)

func TestCheckResponse(t *testing.T) {
	var table = []struct {
		Response *http.Response
		Err      error
	}{
		{
			Response: &http.Response{
				Status:     "200 OK",
				StatusCode: 200,
			},
			Err: nil,
		},
		{
			Response: &http.Response{
				Status:     "404 Not Found",
				StatusCode: 404,
			},
			Err: errors.New(fmt.Sprintf("Incorrect http status code. Status code: 404. Status: 404 Not Found")),
		},
	}
	for _, tt := range table {
		err := CheckResponse(tt.Response)
		if err != nil && err.Error() != tt.Err.Error() {
			t.Fatalf("Got err %s, want err %s", err, tt.Err)
		}
	}
}
