package main

import (
	"errors"
	"fmt"
	"net/http"
	"os"
	"testing"

	gomock "github.com/golang/mock/gomock"
	"github.com/google/go-cmp/cmp"
	"github.com/xanzy/go-gitlab"
)

func TestPipelineClientSetProjectId(t *testing.T) {
	var table = []struct {
		ProjectId    string
		ProjectName  string
		PipelineTask *PipelineTask
		Err          error
	}{
		{
			ProjectId:   "123456",
			ProjectName: "",
			PipelineTask: NewPipelineTask([]byte(`
		  {
				"project": {
					"id": 123456
				},
				"ref": "master",
				"variables": {
					"my_special_var": "abc123"
				}
			}
			`)),
			Err: nil,
		},
		{
			ProjectId:   "mygroup/my-awesome-project",
			ProjectName: "my-awesome-project",
			PipelineTask: NewPipelineTask([]byte(`
			{
				"project": {
					"path": "mygroup/my-awesome-project"
				},
				"ref": "master",
				"variables": {
					"my_special_var": "abc123"
				}
			}
			`)),
			Err: nil,
		},
		{
			ProjectId:   "",
			ProjectName: "",
			PipelineTask: NewPipelineTask([]byte(`
		  {
				"project": {
					"path": ""
				},
				"ref": "master",
				"variables": {
					"my_special_var": "abc123"
				}
			}
			`)),
			Err: fmt.Errorf(
				"Could not set project ID from project data id %v or path %s. Invalid data",
				0, ""),
		},
		{
			ProjectId:   "",
			ProjectName: "",
			PipelineTask: NewPipelineTask([]byte(`
		  {
				"project": {
					"path": "mygroup"
				},
				"ref": "master",
				"variables": {
					"my_special_var": "abc123"
				}
			}
			`)),
			Err: errors.New("Could not set project ID from path mygroup. Invalid path"),
		},
	}
	for _, tt := range table {
		pc := &PipelineClient{}
		err := pc.SetProjectId(tt.PipelineTask)
		if tt.PipelineTask.ProjectId != tt.ProjectId {
			t.Fatalf(
				"Got project ID %s, want project ID %v",
				tt.PipelineTask.ProjectId, tt.ProjectId,
			)
		}
		if err != nil && err.Error() != tt.Err.Error() {
			t.Fatalf("Got err %s, want err %s", err, tt.Err)
		}
	}
}

func TestPipelineClientValidateRef(t *testing.T) {
	mux, server := initTestGitlab()
	defer teardownTestGitlab(server)

	mux.HandleFunc("/api/v4/projects/123456/repository/branches", func(w http.ResponseWriter, r *http.Request) {
		testMethod(t, r, "GET")
		fmt.Fprint(w, `[{"name":"master"},{"name":"develop"}]`)
	})
	var table = []struct {
		PipelineTask *PipelineTask
		Err          error
	}{
		{
			PipelineTask: NewPipelineTask([]byte(`
		  {
				"project": {
					"id": 123456
				},
				"ref": "master",
				"variables": {
					"my_special_var": "abc123"
				}
			}
			`)),
			Err: nil,
		},
		{
			PipelineTask: NewPipelineTask([]byte(`
		  {
				"project": {
					"id": 123456
				},
				"ref": "non-existent-branch",
				"variables": {
					"my_special_var": "abc123"
				}
			}
			`)),
			Err: errors.New("Branch non-existent-branch not found!"),
		},
	}
	for _, tt := range table {
		git, _ := gitlab.NewClient("", gitlab.WithBaseURL(server.URL))
		pc := &PipelineClient{Client: git}
		pc.SetProjectId(tt.PipelineTask)
		err := pc.ValidateRef(tt.PipelineTask)
		if err != nil && err.Error() != tt.Err.Error() {
			t.Fatalf(
				"Got %s, want %s",
				err, tt.Err,
			)
		}
	}
}

func TestPipelineClientGetTrigger(t *testing.T) {
	var table = []struct {
		GitlabResponse string
		Trigger        *gitlab.PipelineTrigger
	}{
		{
			GitlabResponse: `
			[
				{
					"id":10,
					"description":"concordTrigger"
				},
				{
					"id":11,
					"description": "myOtherTrigger"
				}
			]`,
			Trigger: &gitlab.PipelineTrigger{ID: 10, Description: "concordTrigger"},
		},
		{
			GitlabResponse: `[]`,
			Trigger:        &gitlab.PipelineTrigger{ID: 15, Description: "concordTrigger"},
		},
	}
	for _, tt := range table {
		mux, server := initTestGitlab()
		defer teardownTestGitlab(server)

		mux.HandleFunc("/api/v4/projects/123456/triggers", func(w http.ResponseWriter, r *http.Request) {
			if r.Method == "GET" {
				testMethod(t, r, "GET")
				fmt.Fprint(w, tt.GitlabResponse)
			}
			if r.Method == "POST" {
				testMethod(t, r, "POST")
				fmt.Fprint(w, `{"id":15,"description":"concordTrigger"}`)
			}
		})
		git, _ := gitlab.NewClient("", gitlab.WithBaseURL(server.URL))
		pc := &PipelineClient{Client: git}
		got, _ := pc.GetTrigger(123456)
		if !cmp.Equal(got, tt.Trigger) {
			t.Fatalf(
				"Got trigger %v, want trigger %v",
				got, tt.Trigger,
			)
		}
	}
}

func TestPipelineClientTriggerPipeline(t *testing.T) {
	var table = []struct {
		PipelineTask *PipelineTask
		PipelineResp string
		Pipeline     *gitlab.Pipeline
		Err          error
	}{
		{
			PipelineTask: NewPipelineTask([]byte(`
		  {
				"project": {
					"id": 123456
				},
				"ref": "master",
				"variables": {
					"my_special_var": "abc123"
				}
			}
			`)),
			PipelineResp: `{"id": 1, "status": "pending"}`,
			Pipeline:     &gitlab.Pipeline{ID: 1, Status: "pending"},
			Err:          nil,
		},
		{
			PipelineTask: NewPipelineTask([]byte(`
		  {
				"project": {
					"id": 123456
				},
				"ref": "master",
				"variables": {
					"my_special_var": "abc123"
				}
			}
			`)),
			PipelineResp: `{"id": 1, "status": "started"}`,
			Pipeline:     &gitlab.Pipeline{ID: 1, Status: "started"},
			Err:          nil,
		},
	}
	for _, tt := range table {
		mux, server := initTestGitlab()
		defer teardownTestGitlab(server)

		mux.HandleFunc("/api/v4/projects/123456/triggers", func(w http.ResponseWriter, r *http.Request) {
			if r.Method == "GET" {
				testMethod(t, r, "GET")
				fmt.Fprint(w, `[{"id":10, "description":"concordTrigger"}, {"id":11, "description": "myOtherTrigger"}]`)
			}
		})
		mux.HandleFunc("/api/v4/projects/123456/trigger/pipeline", func(w http.ResponseWriter, r *http.Request) {
			testMethod(t, r, "POST")
			fmt.Fprint(w, tt.PipelineResp)
		})

		git, _ := gitlab.NewClient("", gitlab.WithBaseURL(server.URL))
		pc := &PipelineClient{Client: git}
		pc.SetProjectId(tt.PipelineTask)
		got, err := pc.TriggerPipeline(tt.PipelineTask)
		if !cmp.Equal(got, tt.Pipeline) {
			t.Fatalf("Got pipeline %s, want pipeline %s", got, tt.Pipeline)
		}
		if err != nil && err.Error() != tt.Err.Error() {
			t.Fatalf("Got err %s, want err %s", err, tt.Err)
		}
	}
}

func TestPipelineClientStartTask(t *testing.T) {
	ctrl := gomock.NewController(t)
	var table = []struct {
		ConcordTask           *ConcordTask
		PipelineTriggerStatus int
		PipelineTriggerResp   string
		PipelineResp          string
		Err                   error
	}{
		{
			ConcordTask: &ConcordTask{
				Id:      "eac1dbb7-0d75-4943-9656-bf4bb3966111",
				Key:     "host1.example.com",
				Service: "gitlab",
				Type:    "pipeline",
				Async:   false,
				Retries: 2,
				Status:  "pending",
				Options: []byte(`
				{
					"project": {
        			"id": 123456
        	},
        	"ref": "develop",
        	"variables": {
        		"MYGITLABVAR1": "value1",
        		"MYGITLABVAR2": "value2"
        	}
				}
			`),
			},
			PipelineTriggerStatus: http.StatusOK,
			PipelineTriggerResp:   `{"id": 1, "status": "pending"}`,
			PipelineResp:          `{"id": 1, "status": "success"}`,
			Err:                   nil,
		},
		{
			ConcordTask: &ConcordTask{
				Id:      "eac1dbb7-0d75-4943-9656-bf4bb3966111",
				Key:     "host1.example.com",
				Service: "gitlab",
				Type:    "pipeline",
				Async:   false,
				Retries: 2,
				Status:  "pending",
				Options: []byte(`
				{
					"project": {
        			"id": 123456
        		},
        	"ref": "develop",
        	"variables": {
        		"MYGITLABVAR1": "value1",
        		"MYGITLABVAR2": "value2"
        	}
				}
			`),
			},
			PipelineTriggerStatus: http.StatusOK,
			PipelineTriggerResp:   `{"id": 1, "status": "pending"}`,
			PipelineResp:          `{"id": 1, "status": "failed"}`,
			Err:                   nil,
		},
		{
			ConcordTask: &ConcordTask{
				Id:      "eac1dbb7-0d75-4943-9656-bf4bb3966111",
				Key:     "host1.example.com",
				Service: "gitlab",
				Type:    "pipeline",
				Async:   false,
				Retries: 2,
				Status:  "pending",
				Options: []byte(`
				{
					"project": {
        			"id": 123456
        		},
        	"ref": "non-existent-branch",
        	"variables": {
        		"MYGITLABVAR1": "value1",
        		"MYGITLABVAR2": "value2"
        	}
				}
			`),
			},
			PipelineTriggerStatus: http.StatusOK,
			PipelineTriggerResp:   `{"id": 1, "status": "pending"}`,
			PipelineResp:          `{"id": 1, "status": "failed"}`,
			Err:                   errors.New("Branch non-existent-branch not found!"),
		},
		{
			ConcordTask: &ConcordTask{
				Id:      "eac1dbb7-0d75-4943-9656-bf4bb3966111",
				Key:     "host1.example.com",
				Service: "gitlab",
				Type:    "pipeline",
				Async:   false,
				Retries: 2,
				Status:  "pending",
				Options: []byte(`
				{
					"project": {
        			"id": 123456
        		},
        	"ref": "develop",
        	"variables": {
        		"MYGITLABVAR1": "value1",
        		"MYGITLABVAR2": "value2"
        	}
				}
			`),
			},
			PipelineTriggerStatus: http.StatusInternalServerError,
			PipelineTriggerResp:   `{"id": 1, "status": "pending"}`,
			PipelineResp:          "",
			Err:                   errors.New("Could not trigger pipeline!"),
		},
	}
	for _, tt := range table {
		broker := NewMockServiceBroker(ctrl)
		broker.
			EXPECT().
			Call(gomock.Any(), gomock.Any(), gomock.Any()).
			Return(0.0, nil).
			Times(2)
		mux, server := initTestGitlab()
		defer teardownTestGitlab(server)

		mux.HandleFunc("/api/v4/projects/123456/triggers", func(w http.ResponseWriter, r *http.Request) {
			if r.Method == "GET" {
				testMethod(t, r, "GET")
				fmt.Fprint(w, `[{"id":10, "description":"concordTrigger"}, {"id":11, "description": "myOtherTrigger"}]`)
			}
		})
		mux.HandleFunc("/api/v4/projects/123456/repository/branches", func(w http.ResponseWriter, r *http.Request) {
			testMethod(t, r, "GET")
			fmt.Fprint(w, `[{"name":"master"},{"name":"develop"}]`)
		})
		mux.HandleFunc("/api/v4/projects/123456/trigger/pipeline", func(w http.ResponseWriter, r *http.Request) {
			testMethod(t, r, "POST")
			w.WriteHeader(tt.PipelineTriggerStatus)
			w.Write([]byte(tt.PipelineResp))
		})
		mux.HandleFunc("/api/v4/projects/123456/pipelines/1", func(w http.ResponseWriter, r *http.Request) {
			testMethod(t, r, "GET")
			fmt.Fprint(w, tt.PipelineResp)
		})
		concordClient := ConcordClient{broker}
		git, _ := gitlab.NewClient("", gitlab.WithBaseURL(server.URL))
		pc := &PipelineClient{Client: git, ConcordClient: concordClient}
		os.Setenv("GITLAB_API_RETRIES", "2")
		err := pc.StartTask(tt.ConcordTask)
		if err != nil && err.Error() != tt.Err.Error() {
			t.Fatalf("Got err %s, want err %s", err, tt.Err)
		}
	}
}

func TestPipelineClientCancelTask(t *testing.T) {
	ctrl := gomock.NewController(t)
	var table = []struct {
		ConcordTask          *ConcordTask
		ListPipelineResp     string
		PipelineCancelStatus int
		PipelineCancelResp   string
		Err                  error
	}{
		{
			ConcordTask: &ConcordTask{
				Id:      "eac1dbb7-0d75-4943-9656-bf4bb3966111",
				Key:     "host1.example.com",
				Service: "gitlab",
				Type:    "pipeline",
				Async:   false,
				Retries: 2,
				Status:  "pending",
				Options: []byte(`
				{
					"project": {
        			"id": 123456
        	},
        	"ref": "develop",
        	"variables": {
        		"MYGITLABVAR1": "value1",
        		"MYGITLABVAR2": "value2"
        	}
				}
			`),
			},
			ListPipelineResp:     `[{"id": 1, "status": "pending"}]`,
			PipelineCancelStatus: http.StatusOK,
			PipelineCancelResp:   `{"id": 1, "status": "canceled"}`,
			Err:                  nil,
		},
		{
			ConcordTask: &ConcordTask{
				Id:      "eac1dbb7-0d75-4943-9656-bf4bb3966111",
				Key:     "host1.example.com",
				Service: "gitlab",
				Type:    "pipeline",
				Async:   false,
				Retries: 2,
				Status:  "running",
				Options: []byte(`
				{
					"project": {
        			"id": 123456
        		},
        	"ref": "develop",
        	"variables": {
        		"MYGITLABVAR1": "value1",
        		"MYGITLABVAR2": "value2"
        	}
				}
			`),
			},
			ListPipelineResp:     `[{"id": 1, "status": "running"}]`,
			PipelineCancelStatus: http.StatusOK,
			PipelineCancelResp:   `{"id": 1, "status": "canceled"}`,
			Err:                  nil,
		},
		{
			ConcordTask: &ConcordTask{
				Id:      "eac1dbb7-0d75-4943-9656-bf4bb3966111",
				Key:     "host1.example.com",
				Service: "gitlab",
				Type:    "pipeline",
				Async:   false,
				Retries: 2,
				Status:  "pending",
				Options: []byte(`
				{
					"project": {
        			"id": 123456
        		},
        	"ref": "develop",
        	"variables": {
        		"MYGITLABVAR1": "value1",
        		"MYGITLABVAR2": "value2"
        	}
				}
			`),
			},
			ListPipelineResp:     `[{"id": 1, "status": "running"}]`,
			PipelineCancelStatus: http.StatusInternalServerError,
			PipelineCancelResp:   "",
			Err:                  errors.New("Could not cancel task!"),
		},
	}
	for _, tt := range table {
		broker := NewMockServiceBroker(ctrl)
		broker.
			EXPECT().
			Call(gomock.Any(), gomock.Any(), gomock.Any()).
			Return(0.0, nil).
			Times(2)
		mux, server := initTestGitlab()
		defer teardownTestGitlab(server)

		mux.HandleFunc("/api/v4/projects/123456/pipelines", func(w http.ResponseWriter, r *http.Request) {
			if r.Method == "GET" {
				testMethod(t, r, "GET")
				fmt.Fprint(w, `[{"id":1, "status": "running"}]`)
			}
		})
		mux.HandleFunc("/api/v4/projects/123456/pipelines/1/variables", func(w http.ResponseWriter, r *http.Request) {
			testMethod(t, r, "GET")
			fmt.Fprint(w, `[{"key":"concord_task_id", "value":"eac1dbb7-0d75-4943-9656-bf4bb3966111"}]`)
		})
		mux.HandleFunc("/api/v4/projects/123456/pipelines/1/cancel", func(w http.ResponseWriter, r *http.Request) {
			testMethod(t, r, "POST")
			w.WriteHeader(tt.PipelineCancelStatus)
			w.Write([]byte(tt.PipelineCancelResp))
		})
		concordClient := ConcordClient{broker}
		git, _ := gitlab.NewClient("", gitlab.WithBaseURL(server.URL))
		pc := &PipelineClient{Client: git, ConcordClient: concordClient}
		os.Setenv("GITLAB_API_RETRIES", "2")
		err := pc.CancelTask(tt.ConcordTask)
		if err != nil && err.Error() != tt.Err.Error() {
			t.Fatalf("Got err %v, want err %v", err, tt.Err)
		}
	}
}
