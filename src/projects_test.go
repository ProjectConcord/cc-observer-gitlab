package main

import (
	"net/http"
	"os"
	"testing"

	gomock "github.com/golang/mock/gomock"
	"github.com/xanzy/go-gitlab"
)

func TestProjectClientStartTask(t *testing.T) {
	ctrl := gomock.NewController(t)
	var table = []struct {
		ConcordTask    *ConcordTask
		GetProjectResp string
		Err            error
	}{
		{
			ConcordTask: &ConcordTask{
				Id:      "eac1dbb7-0d75-4943-9656-bf4bb3966111",
				Key:     "my-special-project",
				Service: "gitlab",
				Type:    "project",
				Retries: 2,
				Status:  "pending",
				Options: []byte(`
			{
					"project_namespace": "mygroup",
					"project_name": "my-special-project"
			}`),
			},
			GetProjectResp: `
			{
				"id": 1,
				"path": "mygroup/my-special-project",
				"name": "my-special-project"
			}`,
			Err: nil,
		},
		{
			ConcordTask: &ConcordTask{
				Id:      "eac1dbb7-0d75-4943-9656-bf4bb3966111",
				Key:     "my-special-project",
				Service: "gitlab",
				Type:    "project",
				Retries: 2,
				Status:  "pending",
				Options: []byte(`
			{
					"project_namespace": "mygroup",
					"project_name": "my-special-project"
			}`),
			},
			GetProjectResp: "",
			Err:            nil,
		},
	}
	for _, tt := range table {
		broker := NewMockServiceBroker(ctrl)
		broker.
			EXPECT().
			Call(gomock.Any(), gomock.Any(), gomock.Any()).
			Return(0.0, nil).
			Times(2)
		mux, server := initTestGitlab()
		defer teardownTestGitlab(server)

		mux.HandleFunc("/api/v4/projects", func(w http.ResponseWriter, r *http.Request) {
			if r.Method == "GET" {
				w.Write([]byte(tt.GetProjectResp))
			}
			if r.Method == "POST" {
				w.Write([]byte(`{"id":1,"name": "my-special-project"}`))
			}
			if r.Method == "PUT" {
				w.Write([]byte(`{"id":1,"name": "my-special-project"}`))
			}
		})
		mux.HandleFunc("/api/v4/namespaces/", func(w http.ResponseWriter, r *http.Request) {
			if r.Method == "GET" {
				testMethod(t, r, "GET")
				w.Write([]byte(`[
					{
						"id":1,
						"name": "mygroup",
						"path": "mygroup",
						"full_path":"mygroup"
					}
				]`))
			}
		})
		concordClient := ConcordClient{broker}
		git, _ := gitlab.NewClient("", gitlab.WithBaseURL(server.URL))
		pc := &ProjectClient{Client: git, ConcordClient: concordClient}
		os.Setenv("GITLAB_API_RETRIES", "2")
		err := pc.StartTask(tt.ConcordTask)
		if err != nil && err != tt.Err {
			t.Fatalf("Got err %s, want err %s", err, tt.Err)
		}
	}
}
