module gitlab.com/ProjectConcord/cc-observer-gitlab/src/main

go 1.15

require (
	github.com/bitwurx/jrpc2 v0.0.0-20200508153510-d8310ad1baf0
	github.com/golang/mock v1.4.4
	github.com/google/go-cmp v0.5.2
	github.com/gorilla/websocket v1.4.2
	github.com/xanzy/go-gitlab v0.38.1
)
