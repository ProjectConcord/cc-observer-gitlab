package main

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"github.com/bitwurx/jrpc2"
	"github.com/xanzy/go-gitlab"
)

const (
	BrokerCallErrorCode jrpc2.ErrorCode = -32100 // broker call jrpc error code.
	GitLabBaseUrl                       = "GITLAB_BASE_URL"
	GitLabTokenEnv                      = "GITLAB_TOKEN"
	TaskStatusError                     = "error"
	TaskStatusCompleted                 = "completed"
)

var (
	ControllerApi         = os.Getenv("CONCORD_CONTROLLER_API")
	InsecureTls           = GetEnvBool("INSECURE_TLS", false)
	PipelineNotFoundError = errors.New("No pipeline could be found with that name/id")
)

type PipeStatus struct {
	Status        string
	ConcordStatus string
	IsFinished    bool
}

type ConcordClient struct {
	broker ServiceBroker
}

type GitlabClient interface {
	HandleTaskError(error, *ConcordTask)
	StartTask(*ConcordTask) error
	CancelTask(*ConcordTask) error
}

// ServiceBroker contains method for calling external services.
type ServiceBroker interface {
	Call(string, string, map[string]interface{}) (interface{}, *jrpc2.ErrorObject)
}

// JsonRPCServiceBroker is json-rpc 2.0 service broker.
type JsonRPCServiceBroker struct{}

// Concord specific task data derived from Event.meta
type ConcordTask struct {
	Id      string          `json:"_id"`
	Status  string          `json:"_status"`
	Key     string          `json:"_key"`
	Service string          `json:"service"`
	Type    string          `json:"type"`
	Async   bool            `json:"async"` // Toggles whether the observer should watch and handle status.
	Retries int             `json:"retries"`
	Options json.RawMessage `json:"options"`
}

func CheckResponse(resp *http.Response) error {
	switch resp.StatusCode {
	case 200, 201, 202, 204, 304:
		return nil
	}
	err := errors.New(fmt.Sprintf(
		"Incorrect http status code. Status code: %v. Status: %s",
		resp.StatusCode, resp.Status))
	return err
}

func (broker *JsonRPCServiceBroker) Call(
	url string,
	method string,
	params map[string]interface{}) (interface{}, *jrpc2.ErrorObject) {

	p, _ := json.Marshal(params)

	req := bytes.NewBuffer([]byte(fmt.Sprintf(
		`{
			"jsonrpc": "2.0",
			"method": "%s", "params": %s,
			"id": 0}`, method, string(p))))
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: InsecureTls},
	}
	client := &http.Client{Transport: tr}
	resp, err := client.Post(fmt.Sprintf(
		"%s/rpc", url), "application/json", req)
	if err != nil {
		return nil, &jrpc2.ErrorObject{
			Code:    BrokerCallErrorCode,
			Message: jrpc2.ServerErrorMsg,
			Data:    err.Error(),
		}
	}
	err = CheckResponse(resp)
	if err != nil {
		return nil, &jrpc2.ErrorObject{
			Code:    BrokerCallErrorCode,
			Message: jrpc2.ServerErrorMsg,
			Data:    err.Error(),
		}
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, &jrpc2.ErrorObject{
			Code:    BrokerCallErrorCode,
			Message: jrpc2.ServerErrorMsg,
			Data:    err.Error(),
		}
	}

	var respObj jrpc2.ResponseObject
	json.Unmarshal(body, &respObj)

	return respObj.Result, respObj.Error
}

func ConcordStatus(status string) string {
	concordStates := map[string]string{
		"failed":    "error",
		"cancelled": "cancelled",
		"canceled":  "cancelled",
		"success":   "complete",
		"manual":    "manual",
		"skipped":   "skipped",
	}

	return concordStates[status]
}

func (c *ConcordClient) RequestAck(key string) error {
	if key == "" {
		return fmt.Errorf("key is null!")
	}
	params := map[string]interface{}{"key": key}
	result, errObj := c.broker.Call(ControllerApi, "startTask", params)
	if errObj != nil {
		log.Printf("RequestAck err in startTask. msg: %s data: %s", string(errObj.Message), errObj.Data.(string))
		return fmt.Errorf("Failed to start Concord task for key %s", key)
	}
	result = int(result.(float64))

	// A succes result code should be 0
	// Anything else means this is not an actionable event
	if result != 0 {
		return fmt.Errorf("Result code was not 0 %v", result)
	}

	return nil

}

func NewGitlabClient(taskType string) (gc GitlabClient, err error) {
	apiToken, err := ValidateEnv(GitLabTokenEnv)
	if err != nil {
		return nil, err
	}
	baseUrl, err := ValidateEnv(GitLabBaseUrl)
	if err != nil {
		return nil, err
	}
	git, err := gitlab.NewClient(apiToken, gitlab.WithBaseURL(baseUrl))
	if err != nil {
		return nil, err
	}

	switch taskType {
	case "pipeline":
		pc := &PipelineClient{Client: git}
		pc.ConcordClient = ConcordClient{broker: &JsonRPCServiceBroker{}}
		gc = pc
	case "project":
		pc := &ProjectClient{Client: git}
		pc.ConcordClient = ConcordClient{broker: &JsonRPCServiceBroker{}}
		gc = pc

	default:
		err = fmt.Errorf(
			"JSON error: Could not find a client for type %s", taskType)
	}
	return gc, err
}

func (c *ConcordClient) HandleTaskError(err error, ct *ConcordTask) {
	log.Printf("Failed to start task in %s err: %s", ct.Id, err)
	ct.Status = TaskStatusError
	c.CompleteTask(ct)
}

func (c *ConcordClient) CompleteTask(ct *ConcordTask) (bool, error) {

	params := map[string]interface{}{"id": ct.Id, "status": ct.Status}

	result, errObj := c.broker.Call(ControllerApi, "completeTask", params)
	if errObj != nil {
		return false, errors.New(string(errObj.Message))
	}
	result = int(result.(float64))

	// A succes result code should be 0
	// Anything else means this is not an actionable event
	if result != 0 {
		return false, errors.New(fmt.Sprintf("Broker call was not 0. Was %v", result))
	}
	log.Printf("Task %s completed with status %s", ct.Id, ct.Status)
	return true, nil
}
