package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/xanzy/go-gitlab"
)

type PipelineClient struct {
	Client *gitlab.Client
	ConcordClient
}

type ProjectData struct {
	Id   int    `json:"id"`
	Path string `json:"path"`
}

type RequestData struct {
	ProjectData *ProjectData      `json:"project"`
	Ref         string            `json:"ref"`
	Retries     int               `json:"retries"`
	Token       string            `json:"token"` // Optional gitlab trigger token be passed in.
	Variables   map[string]string `json:"variables"`
}

type PipelineTask struct {
	RequestData *RequestData
	ProjectId   string
	ProjectName string
}

// NewPipelineTask returns an initialized ConcordTask instance.
func NewPipelineTask(data []byte) *PipelineTask {
	pt := &PipelineTask{}
	err := json.Unmarshal(data, &pt.RequestData)
	if err != nil {
		log.Printf("New task error %s", err)
	}
	return pt
}

func (pc *PipelineClient) SetProjectId(pt *PipelineTask) error {
	// Given an path or id set the "ID" we're going to send to Gitlab.
	// ProjectData.Id is the preference.

	pd := pt.RequestData.ProjectData

	if pd.Id > 0 {
		parsedId, err := parseID(pd.Id)
		if err != nil {
			return err
		}
		pt.ProjectId = parsedId

		return nil
	}

	if pd.Path == "" {
		err := fmt.Errorf(
			"Could not set project ID from project data id %v or path %s. Invalid data",
			pd.Id, pd.Path)
		log.Println(err)

		return err
	}

	pathStrings := strings.Split(pd.Path, "/")
	if len(pathStrings) <= 1 {
		err := fmt.Errorf("Could not set project ID from path %s. Invalid path", pd.Path)
		log.Println(err)

		return err
	}

	pt.ProjectId = pd.Path
	// The project name should be at the end of the path.
	pt.ProjectName = pathStrings[len(pathStrings)-1]

	return nil
}

func (pc *PipelineClient) ValidateRef(pt *PipelineTask) error {
	branches, _, err := pc.Client.Branches.ListBranches(pt.ProjectId, nil, nil)
	if err != nil {
		return err
	}
	if len(branches) < 1 {
		return fmt.Errorf("No branches found in this project")
	}
	for _, branch := range branches {
		if pt.RequestData.Ref == branch.Name {
			log.Printf("Task ref: %s branch name %s", pt.RequestData.Ref, branch.Name)
			return nil
		}
	}
	return errors.New(fmt.Sprintf("Branch %s not found!", pt.RequestData.Ref))
}

func (pc *PipelineClient) CancelTask(ct *ConcordTask) (err error) {
	retries := GetEnvInt("GITLAB_API_RETRIES", DefaultApiRetries)
	var pt PipelineTask
	err = json.Unmarshal(ct.Options, &pt.RequestData)
	if err != nil {
		return err
	}
	err = pc.SetProjectId(&pt)
	if err != nil {
		return err
	}
	var pipelines []*gitlab.PipelineInfo
	scopes := []string{"running", "pending"}
	for _, scope := range scopes {
		opt := &gitlab.ListProjectPipelinesOptions{Scope: gitlab.String(scope)}
		pl, _, err := pc.Client.Pipelines.ListProjectPipelines(pt.ProjectId, opt)
		if err != nil {
			log.Printf("Failed to retrieve pipelines for cancel task. Error: %s", err)
			return err
		}
		pipelines = append(pipelines, pl...)
	}

	for _, pipeline := range pipelines {
		pipelineVars, _, err := pc.Client.Pipelines.GetPipelineVariables(pt.ProjectId, pipeline.ID, nil)
		if err != nil {
			return err
		}
		for _, pipelineVar := range pipelineVars {
			if pipelineVar.Key == "concord_task_id" && pipelineVar.Value == ct.Id {
				err := Retry(retries, time.Second, func() error {
					log.Println("Attempting to cancel Concord pipeline task :", ct.Id)
					cancelled, _, err := pc.Client.Pipelines.CancelPipelineBuild(pt.ProjectId, pipeline.ID)
					if err != nil {
						log.Printf(
							"Failed to cancel task %s. Pipeline ID: %v.\nErr: %v",
							ct.Id, pipeline.ID, err)
						return errors.New("Could not cancel task!")
					}
					log.Printf(
						"Successfully cancelled task %s. Pipeline ID: %v Status %s",
						ct.Id, cancelled.ID, cancelled.Status)
					return nil
				})
				if err != nil {
					return err
				}
			}
		}
	}
	return nil
}

func (pc *PipelineClient) GetTrigger(projectId interface{}) (trigger *gitlab.PipelineTrigger, err error) {
	triggers, _, err := pc.Client.PipelineTriggers.ListPipelineTriggers(projectId, nil)

	for _, trigger := range triggers {
		if trigger.Description == TriggerName {
			log.Printf("Found trigger: Id: %v, description: %s", trigger.ID, trigger.Description)
			return trigger, nil
		}
	}

	log.Println("Trigger not found for Concord. Creating one...")
	opt := &gitlab.AddPipelineTriggerOptions{Description: gitlab.String(TriggerName)}
	trigger, _, err = pc.Client.PipelineTriggers.AddPipelineTrigger(projectId, opt)
	if err != nil {
		return nil, err
	}

	return trigger, err
}

func (pc *PipelineClient) StartTask(ct *ConcordTask) error {
	if ct.Status != "pending" {
		return nil
	}
	retries := GetEnvInt("GITLAB_API_RETRIES", DefaultApiRetries)

	err := Retry(retries, time.Second, func() error {
		err := pc.RequestAck(ct.Key)
		if err != nil {
			return err
		}
		return nil
	})
	if err != nil {
		pc.HandleTaskError(err, ct)
		return err
	}

	var pt PipelineTask
	err = json.Unmarshal(ct.Options, &pt.RequestData)
	if err != nil {
		pc.HandleTaskError(err, ct)
		return err
	}

	err = pc.SetProjectId(&pt)
	if err != nil {
		pc.HandleTaskError(err, ct)
		return err
	}

	// Set the concord_id in trigger vars so we can identify it.
	pt.RequestData.Variables["concord_task_id"] = ct.Id
	pt.RequestData.Variables["concord_resource"] = ct.Key

	if pt.RequestData.Ref == "" {
		pt.RequestData.Ref = DefaultRef
	}
	err = Retry(retries, time.Second, func() error {
		err := pc.ValidateRef(&pt)
		if err != nil {
			return err
		}
		return nil
	})
	if err != nil {
		pc.HandleTaskError(err, ct)
		return err
	}
	pipeline, err := pc.TriggerPipeline(&pt)

	if err != nil {
		pc.HandleTaskError(err, ct)

		return errors.New("Could not trigger pipeline!")
	}

	if !ct.Async {
		status, err := pc.WatchUntilComplete(pt.ProjectId, pipeline.ID, ct.Retries)
		if err != nil {
			return err
		}
		log.Printf("WatchUntilComplete status: %s", status)
		ct.Status = status
		ok, err := pc.CompleteTask(ct)
		if !ok {
			log.Println("Encountered error completing task: ", err)
			return err
		}
		return nil
	}

	return nil
}

func (pc *PipelineClient) GetPipelineStatus(projectId interface{}, pipeLineId int) (string, error) {
	p, _, err := pc.Client.Pipelines.GetPipeline(projectId, pipeLineId)
	if err != nil {
		return "", err
	}

	return p.Status, err
}

func (pc *PipelineClient) TriggerPipeline(pt *PipelineTask) (*gitlab.Pipeline, error) {
	trigger, err := pc.GetTrigger(pt.ProjectId)
	if err != nil {
		return nil, err
	}
	opt := &gitlab.RunPipelineTriggerOptions{
		Ref:       gitlab.String(pt.RequestData.Ref),
		Token:     gitlab.String(trigger.Token),
		Variables: pt.RequestData.Variables}
	log.Printf(
		"Triggering pipeline for project %s on Ref %s",
		pt.ProjectId, pt.RequestData.Ref)
	p, _, err := pc.Client.PipelineTriggers.RunPipelineTrigger(pt.ProjectId, opt)
	if err != nil {
		return nil, err
	}

	return p, nil
}

func (pc *PipelineClient) WatchUntilComplete(projectId interface{}, pipeLineId int, retries int) (string, error) {
	var status string
	for {
		err := Retry(retries, time.Second, func() error {
			s, err := pc.GetPipelineStatus(projectId, pipeLineId)
			if err != nil {
				return err
			}
			status = s
			return nil
		})
		if err != nil {
			log.Printf("Could not retrieve pipeline status. Error: %q", err)
			return ConcordStatus("failed"), err
		}
		log.Println("Pipeline ", pipeLineId, "Status", status)
		if IsFinished(status) {
			if status == "failed" && retries > 0 {
				log.Printf("Pipeline status is %s, retrying attempt %v", status, retries)
				pc.Client.Pipelines.RetryPipelineBuild(projectId, pipeLineId, nil)
				retries--
				continue
			}
			log.Printf("Pipeline %v finished with status %s", pipeLineId, status)
			return ConcordStatus(status), nil
		}
		log.Printf("Pipeline %v is still running. Status: %s", pipeLineId, status)
		time.Sleep(10 * time.Second)
	}
}
