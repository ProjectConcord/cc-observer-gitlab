package main

import "time"

const (
	TriggerName                     = "concordTrigger"
	DefaultRef                      = "master"
	DefaultApiRetries               = 10
	MaxTaskRetries                  = 15
	RetryInterval                   = 30
	PingPeriod        time.Duration = 60 * time.Second
)
